function school(numOfT,category,location,hm,numOfCooks,colorOfUniform,typeOfSchool){
this.numOfT=numOfT;
this.category=category;
this.location=location;
this.hm=hm;
this.numOfCooks=numOfCooks;
this.colorOfUniform=colorOfUniform;
this.typeOfSchool=typeOfSchool;
}

let firstSchool = new school(40,'private','namwongo','Dinton', 10, 'red','mixed')
let secondSchool = new school(50,'government','muyenga','Harold', 20, 'yellow','single')
let thirdSchool = new school(60,'private','kibuli','Dinton', 30, 'blue','mixed')