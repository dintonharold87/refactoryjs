//Defining an object calleed cattle
let cattle = {
    // These are properties of the object cattle with their values.
    breed:"Fresian",
    color:"Black \& white",
    DateOfBirth:"January,1,2019",
    sex:"female",
    weight:"240 kgs",
    cattleId:"DH-100",
    type:"Dairy",
    /*
    These are the methods for the object
    */
    deworming:function(){
        return "Every 6 months"
    },
    vaccination:function(){
        return "Every year"
    },
    milking:function(){
        return "Everyday"
    }
};


//Defining an object calleed shirt
let shirt = {
    // These are properties of the object shirt with their values.
   color:"red",
   material:"silk",
   price:250000,
   label:"Hawes \& Curtis",
   type:"Dress shirt",
   size:"medium",
   countryOfOrigin:"United Kingdom",

    /*
    These are the methods for the object
    */
    washing:function(){
        return "Everytime you put it on"
    },
    Ironing:function(){
        return "Everytime you wash"
    },
    Wearing:function(){
        return "Whenever you have a formal function"
    }
};

//Defining an object calleed trouser
let trouser = {
    // These are properties of the object trouser with their values.
   color:"black",
   material:"cotton",
   price:150000,
   label:"Nike",
   type:"sweat pants",
   size:"medium",
   countryOfOrigin:"USA",

    /*
    These are the methods for the object
    */
    washing:function(){
        return "Everytime you put it on"
    },
    Ironing:function(){
        return "No need to iron"
    },
    Wearing:function(){
        return "Whenever you have a Chilling function"
    }
};

//Defining an object calleed jacket
let jacket = {
    // These are properties of the object jacket with their values.
   color:"grey",
   material:"wool",
   price:500000,
   label:"Umbro",
   type:"Winter",
   size:"medium",
   countryOfOrigin:"USA",

    /*
    These are the methods for the object
    */
    washing:function(){
        return "Dry cleaning"
    },
    Ironing:function(){
        return "No need"
    },
    Wearing:function(){
        return "During Winter"
    }
};

//Defining an object calleed vest
let vest = {
    // These are properties of the object vest with their values.
   color:"black",
   material:"polyester",
   price:25000,
   label:"Under Armour",
   type:"gym vest",
   size:"medium",
   countryOfOrigin:"United Kingdom",

    /*
    These are the methods for the object
    */
    washing:function(){
        return "Everytime you put it on"
    },
    Ironing:function(){
        return "Everytime you wash"
    },
    Wearing:function(){
        return "Whenever you are going for some sports training"
    }
};

//Defining an object calleed shoe
let shoe = {
    // These are properties of the object shoe with their values.
   color:"black",
   material:"leather",
   price:250000,
   label:"Gabaddini",
   type:"Mocassin",
   size:"medium",
   countryOfOrigin:"Italy",

    /*
    These are the methods for the object
    */
    washing:function(){
        return "Everytime they get dirty"
    },
    polishing:function(){
        return "Everytime you are going to put them on"
    },
    Wearing:function(){
        return "Whenever you have a formal function"
    }
};

//Defining an object calleed sweater
let sweater = {
    // These are properties of the object sweater with their values.
   color:"maroon",
   material:"nylon",
   price:250000,
   label:"Adidas",
   type:"turtle neck",
   size:"medium",
   countryOfOrigin:"USA",

    /*
    These are the methods for the object
    */
    washing:function(){
        return "Everytime you put it on"
    },
    Ironing:function(){
        return "No need to iron"
    },
    Wearing:function(){
        return "Whenever you are feeling cold"
    }
};
