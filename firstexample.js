/*** 
//declaration with javascript demo
//JS
let a;
let b;
let c;

var d;
var e;
var f;

// syntax and semantics
//python
a=10
b=20
c=25
//VB
din a=20
din b;
din c;
// C#, C, C++
a;
float b=1.0;
***/
/*** 
let num1=10;
let num2=20;
let ans = num1 + num2;
console.log(ans);
***/
/**
 
let num="2";
let num1=2;
console.log(num == num1);
**/

let num1=20;
 var num2=30;
let num3=40;
let mynums=[10,30,40,5.3,"dintaine",[40,50,[40,30]]];
 //console.log(mynums[5][2][1]);
//Conditions and loops in Javascript
/**if(num1>=num1){
    console.log(num1);
}
else {
    console.log(num2);
}


let ans=num1 < num2? num1:num2;

for(i=1; i<=num1;i++){
    console.log(i);
}
**/

/*
for(i=0; i<=num3; i+=4){
if(i%2 == 0){
    console.log(i);
}
}
*/




function even(){
    for(i=0; i<=num3; i+=4){
        if(i%2 == 0){
            console.log(i);
        }
        }
}
even();

function paye(){
    let pay = 1000000;
    let charge = 30;
    const a =100;
    let total =(charge/a)* pay;
    let totalPay =pay - total;
    return totalPay;
}
function nssf(){
    let pay= 11;
    const b =100;
    let totalNssf=(pay/b)*paye();
    let nssf=paye()-totalNssf;
}

//re-write PAYE
function paye1(pay,charge,a){
    let total =(charge/a)* pay;
    let totalPay =pay - total;
    return totalPay;  
}
//paye1(1000000,30,100);

// function definition with parameters
function nssf1(pay,b){
    let totalNssf=(pay/b)*paye1(1000000,30,100);
    let nssf=paye1(1000000,30,100)-totalNssf;
    console.log(nssf);

}

//function call with arguments
nssf1(11,100);


function tests(test1,test2)
{
// 
let greater = test2>test1?test1:test2
//
//console.log(greater)
return greater

}
tests(80,80)