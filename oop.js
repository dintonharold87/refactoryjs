// how do we achieve object orientation in JS
//class vs objects
//An object is an instance of a class
//A class is a blueprint of an object

let myboject={};
console.log(typeof(myboject))
let food={
    name:"potatoes",
    taste:"sweet",
    price:4000,
    preparation: function(){
        return "fried"
    }
};
console.log(food.preparation())